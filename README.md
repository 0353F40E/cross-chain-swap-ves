# Cross-chain Atomic Swaps With Bitcoin Cash (BCH) Using One-Time Verifiably Encrypted Signatures (VES)

    Author: bitcoincashautist
    Last Edit Date: 2024-03-21

**Abstract.** With advances in cryptography, it became possible to implement atomic swaps between Bitcoin (BTC) and Monero (XMR) and two such protocols have been successfuly demonstrated.
However, the protocols require the scriptable network to have 2nd-party malleability solved and list SegWit as a requirement.
With advances in Bitcoin Cash (BCH) smart contract capabilities, it became possible to implement an equivalent atomic swap protocol despite BCH network not having SegWit.

In this work, we present our solution to cross-chain atomic swaps between Bitcoin Cash (BCH) and Monero (XMR), using adaptor signatures and discrete logarithm equivalence proofs.
Our solution uses newly available native introspection opcodes to commit to transactions contents, making it immune to 2nd-party malleability, and with transaction size significantly reduced compared to previous work.
We also introduce a cross-chain atomic swap between Bitcoin Cash (BCH) and Bitcoin (BTC), using similar approach.

**Keywords:** Blockchain · Atomic Swap · Bitcoin Cash · Monero · Adaptor Signatures.

## Introduction

Cross-chain atomic swaps using Hash Time Locked Contracts (HTLCs)[\[1\]](https://en.bitcoin.it/wiki/Hash_Time_Locked_Contracts), as conceived by TierNolan[\[2\]](https://bitcointalk.org/index.php?topic=193281.msg2224949\#msg2224949), have been around for a while.
The first practical implementation[\[3\]](https://github.com/decred/atomicswap) was in 2017, between Litecoin (LTC) and Decred (DCR).
Such swaps require both networks to primarily have two smart contract building blocks:

- hashlocks, with which a contract can be set to require that the spender provides some data whose hash will match the one set in the contract, and
- timelocks, with which a contract can be set to require a specific time to be reached before allowing spending.

With later invention of adaptor signatures[\[4\]](https://www.sosthene.net/wp-content/uploads/wpforo/default_attachments/1562671969-Poelstra-Scriptless-Scripts.pdf), also known as verifiably encrypted signatures[\[5\]](https://raw.githubusercontent.com/LLFourn/one-time-VES/master/main.pdf), it became possible to create cross-chain atomic swaps based on Point Time Locked Contracts (PTLCs)[\[6\]](https://bitcoinops.org/en/topics/ptlc/).
Those contracts avoid the need for a hashlock, because it will be the signature itself that reveals a secret to the other party, allowing it to spend the coins on the other network.
The first practical implementation[\[7\]](https://github.com/comit-network/grin-btc-poc) was in 2019, between Bitcoin (BTC) and Grin (GRIN).

Finally, with the work of Gugger[\[8\]](https://eprint.iacr.org/2020/1126.pdf), and later Hoenisch & Pino[\[9\]](https://arxiv.org/pdf/2101.12332.pdf), the problem of cross-chain atomic swaps was solved even for swaps where one network doesn't have any smart contract capabilities.
Thanks to discrete log equivalence (DLEQ) proofs[\[16\]](https://web.getmonero.org/es/resources/research-lab/pubs/MRL-0010.pdf), the networks can even use different elliptic curves.
It is enough that only one network has the needed smart contract capabilities, and the requirements on the other network are just:

- that it uses compatible public-key cryptography to verify signatures, and
- that transactions are immutable after some time.

The swap introduced a new requirement, that of the scriptable network having the problem of 2nd-party malleability solved.
This is because the refund path needs two transactions, one to initiate it and the other to complete it.
Without solving 2nd-party malleability, the party offering XMR could re-roll their part of multisig signature which initiates refund, and so change the TXID of the refund initiation transaction.
This would block the refund completion path for the party who funded the BTC contract (the path in descendant transaction), allowing the other party to later sweep the BTC when timelock would expire, without having to even temporarily give up control of their XMR.

Bitcoin Cash (BCH) never implemented SegWit[\[10\]](https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki), so we cannot use the exact same contract as in [\[9\]](https://arxiv.org/pdf/2101.12332.pdf) because, on the BCH network, it would be vulnerable to 2nd-party malleability.
In the meantime, the BCH network was upgraded with OP_CHECKDATASIGVERIFY[\[11\]](https://documentation.cash/protocol/forks/op_checkdatasig.html) (2018, abbreviated CDS, also known as check-signature-from-stack, abbreviated CSFS) and introspection opcodes[\[12\]](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md) (2022).
With that, we can mitigate malleability by making the refund transaction malleable.
This way the prevout reference can be independently updated should the parent's TXID be changed, and the update would not invalidate the refund path.
This is similar to how Poon & Dryja[\[13\]](https://lightning.network/lightning-network-paper.pdf) originally proposed to address malleability on BTC (SIGHASH_NOINPUT), for the Lightning Network application.

In this work we present our solution to cross-chain atomic swaps between Bitcoin Cash (BCH) and Monero (XMR) using adaptor signatures and discrete logarithm equivalence proofs.
The solution uses newly available BCH smart contract capabilities, making the solution immune to 2nd-party malleability, and with transaction size significantly reduced.

Additionally, we present a cross-chain atomic swap between Bitcoin Cash (BCH) and Bitcoin (BTC) using adaptor signatures.
The solution requires only a minimal PTLC contract on BTC side, and with transaction size being reduceable to a single key spend if Taproot (BIP-341)[\[17\]](https://en.bitcoin.it/wiki/BIP_0341) would be used for the happy path.

## Cross-chain Swaps Between BCH and XMR

The protocol described in this section is largely based on the work of Hoenisch & Pino[\[9\]](https://arxiv.org/pdf/2101.12332.pdf).
Transaction flow is exactly the same, and the main difference is in how the flow is enforced on BCH side of things.

![image](https://gist.github.com/assets/80100588/2417a762-28bd-467e-8d86-e390443f8b0c)

**Figure 1** - Transaction schema for swaps between BCH and XMR. *Top: transactions on BCH network. Bottom: transactions on XMR network.*

The signatures on BCH side involved in VES (highlighted above in bold) will be verified by using OP_CHECKDATASIGVERIFY[\[11\]](https://documentation.cash/protocol/forks/op_checkdatasig.html) and must sign a fixed message committed in the contract, as opposed to signing transaction contents built by the SIGHASH algorithm when OP_CHECKMULTISIG is used.
The purpose of the fixed message signature is similar to the purpose of hashlock in HTLCs: just to open the spending path.
It will be the contract code in that spending path that enforces transaction contents directly, by using introspection opcodes[\[12\]](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md).

### Situation

Alice has some `amount_xmr` and wants to receive some `amount_bch` in exchange.
Bob has some `amount_bch` and wants to receive some `amount_xmr` in exchange.
Alice and Bob discovered each other through some match-making platform and agreed to go through with the exchange.

### Preparation

Alice and Bob exchange commitments to BCH contract parameters they will use for the exchange.
This is required in order to mitigate P2SH weakness to collision search[\[14\]](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#322-plausibility-of-a-collision-attack) while keeping BCH outputs smaller.
Alternatively, they could skip this step and use P2SH32 addresses[\[15\]](https://gitlab.com/0353F40E/p2sh32) for the two BCH contracts.

Then, they exchange a set of addresses, keys, zero-knowledge proofs, and signatures.

Upon verification of exchanged data, Bob initiates the on-chain procedure by broadcasting `tx_bch_swaplock` which locks up his `amount_bch` in a Point Time Locked Contract (PTLC) `swaplock`, defined by the following parameters:

- `mining_fee_0` - pre-agreed mining fee,
- `bytecode_alice` - Alice's payout address bytecode which will receive the `amount_bch`,
- `pubkey_ves_bob_bch` - Bob's one-time pubkey used for VES,
- `timelock_0` - expiry of this timelock will allow refund to be initiated,
- `bytecode_refund` - refund contract bytecode which will receive the `amount_bch` if refund path is used.

### Happy Path

Alice monitors BCH mempool, observes Bob's `tx_bch_swaplock`, and then waits until the transaction gets `confirmations_bch` confirmations.

Alice then commits to the swap by broadcasting `tx_xmr_swaplock` which sends her `amount_xmr` to an XMR output constructed from `pubkey_ves_xmr_spend = pubkey_ves_alice_xmr_spend + pubkey_ves_bob_xmr_spend` and `pubkey_xmr_view = pubkey_alice_xmr_view + pubkey_bob_xmr_view`.

Both `pubkey_ves_bob_xmr` and `pubkey_ves_bob_bch` shall correspond to the same `seckey_ves_bob` even though the pubkeys are generated using different curves.
Same shall hold for Alice's VES keys.
These relationships between keys will be proven during preparation, through the use of Discrete Logarithm Equivalence (DLEQ) proofs[\[16\]](https://web.getmonero.org/es/resources/research-lab/pubs/MRL-0010.pdf).

Bob monitors XMR mempool, observes Alice's `tx_xmr_swaplock`, and then waits until the transaction gets `confirmations_xmr` confirmations.

Bob then uses his `seckey_ves_bob` and `pubkey_ves_alice` to produce the verifiably encrypted signature (VES) `bob_allow_swap_ves`, and he then reveals the VES to Alice.

Alice uses her `seckey_ves_alice` to decrypt the VES and generate `bob_allow_swap_dec`, and she then publishes the `tx_bch_swaplock_swap` which sends BCH from the `swaplock` contract to pre-determined destination of Alice's choice, i.e. an output locked with `bytecode_alice`.

Bob monitors BCH mempool, observes Alice's `tx_bch_swaplock_swap`, extracts the `bob_allow_swap_dec` from the TX, and then uses the decrypted signature to learn Alice's `seckey_ves_alice`.

Bob then uses his `seckey_ves_bob` and `seckey_ves_alice` to send the XMR to destination of his choice and with this the swap is completed.

There's an important distinction between here used `bob_allow_swap_dec` and the one generated in procedure used in [\[9\]](https://arxiv.org/pdf/2101.12332.pdf): the signature here used must sign a pre-determined message committed to by the contract bytecode, verified by OP_CHECKDATASIGVERIFY instead of OP_CHECKSIGVERIFY.
The role of the signature here is just to open the spending path while the TX contents are committed to by the contract code itself instead of by the signature.
This makes the `bob_allow_swap_dec` replayable for any number of `swap` contract UTXOs with the same parameters, since the message to be signed would be exactly the same.
This is OK, because the `seckey_ves_alice` and `seckey_ves_bob` keys are one-time use for this particular swap.

### Refund Path

Either party has a chance of forcing a refund by blocking the happy path and waiting for `timelock_0` expire:

1. If Alice never funds the XMR address,
2. If Bob never reveals the `bob_allow_swap_ves`,
3. If Alice never uses the `bob_allow_swap_dec`.

Once `timelock_0` expires, anyone with knowledge of `swaplock` contract can post the `tx_bch_swaplock_refund_start` transaction to initiate refund.
It could be Alice, Bob, or anyone with whom they shared contract parameters, e.g. the DEX platform automating this part as a service.
The transaction forwards `amount_bch - mining_fee_0` BCH value to `refund` contract, another PTLC of exact same structure as `swaplock` but with different contract parameters:

- `mining_fee_1` - pre-agreed mining fee,
- `bytecode_bob` - Bob's payout address bytecode which will receive the `amount_bch`,
- `pubkey_ves_alice_bch` - Alice's one-time pubkey used for VES,
- `timelock_1` - expiry of this timelock will allow the recover path to be used,
- `bytecode_alice` - Alice's payout address bytecode which will receive the `amount_bch` if recover path is used.

To complete refund, now it will be Bob who decrypts Alice's VES to generate `alice_allow_refund_dec` and open the spending path that will send `amount_bch - mining_fee_0 - mining_fee_1` BCH value to his `bytecode_bob`.
Note that Bob will already have the `alice_allow_refund_enc`, because obtaining it is part of preparation, so he doesn't have to depend on Alice for this step.
Bob will broadcast the `tx_bch_refund_complete` and receive the refunded BCH amount.

Alice will observe the TX, extract `alice_allow_refund_dec` from it and use it to learn Bob's `seckey_ves_bob`.

Alice will then use her `seckey_ves_alice` and `seckey_ves_bob` to send the XMR to destination of her choice and with this the refund is completed.

### Recover Path

It is possible for Bob to block regular refund completion by never publishing the `alice_allow_refund_dec`.

In that case `timelock_1` would expire and open the recover path, so Alice can submit the `tx_bch_refund_recover` and have the BCH forwarded to her `bytecode_alice` instead of Bob's `bytecode_bob`.
Use of this spending path will not leak Alice's `seckey_ves_alice` to Bob, who will be punished because he will not get the `amount_xmr`, creating the incentive for him to complete regular refund on time.

Result of this is that from Alice's point-of-view it will be as if the swap went through.

Bob could get the `amount_xmr` only if Alice would show forgiveness afterwards and directly reveal her `seckey_ves_alice` to Bob.

## Cross-chain Swaps Between BCH and BTC

The protocol described in this section is a modified variant of the above protocol, taking advantage of BTC's scriptability.

![image](https://gist.github.com/assets/80100588/f8854f50-8c8a-44a4-a2a9-82e3a4cadc5b)

**Figure 2** - Transaction schema for swaps between BCH and BTC. *Top: transactions on BCH network. Bottom: transactions on BTC network.*

The happy path will function exactly the same: Alice spending the BCH from `swaplock` will reveal BTC key share to Bob, who can then sweep the BTC.
Because BTC is scriptable, for the refund path we can move `timelock_1` to BTC and eliminate the need for recovery path.

### Preparation

Alice and Bob exchange commitments to BCH and BTC contract parameters they will use for the exchange.

Then, they exchange a set of addresses, keys, zero-knowledge proofs, and signatures.

Upon verification of exchanged data, Bob initiates the on-chain procedure by broadcasting `tx_bch_swaplock` which locks up his `amount_bch` in a Point Time Locked Contract (PTLC) `swaplock`, defined by the following parameters:

- `mining_fee_0` - pre-agreed mining fee,
- `bytecode_alice` - Alice's payout address bytecode which will receive the `amount_bch`,
- `pubkey_ves_bob_bch` - Bob's one-time pubkey used for VES,
- `timelock_0` - expiry of this timelock will allow refund to go through,
- `bytecode_bob` - Bob's payout address bytecode which will receive the `amount_bch` if refund path is used.

If Bob chooses a P2SH address for the refund payout, then the on-chain transaction will look exactly the same as in case of XMR swap.
This is recommended in order to maximize privacy, but there aren't many wallets that can spend from P2SH.
We can help Bob by making `bytecode_bob` a simple covenant that forwards the BCH to Bob's P2PKH address.

### Happy Path

Alice monitors BCH mempool, observes Bob's `tx_bch_swaplock`, and then waits until the transaction gets `confirmations_bch` confirmations.

Alice then commits to the swap by broadcasting `tx_btc_swaplock` which sends her `amount_btc` to a PTLC `swaplock_btc`, defined by the following parameters:

- `pubkey_ves = pubkey_ves_alice + pubkey_ves_bob`
- `timelock_1`
- `pubkey_alice_btc`

Bob monitors BTC mempool, observes Alice's `tx_btc_swaplock`, and then waits until the transaction gets `confirmations_btc` confirmations.

Bob then uses his `seckey_ves_bob` and `pubkey_ves_alice` to produce the verifiably encrypted signature (VES) `bob_allow_swap_ves`, and he then reveals the VES to Alice.

Alice uses her `seckey_ves_alice` to decrypt the VES and generate `bob_allow_swap_dec`, and she then publishes the `tx_bch_swaplock_swap` which sends BCH from the `swaplock` contract to pre-determined destination of Alice's choice, i.e. an output locked with `bytecode_alice`.

Bob monitors BCH mempool, observes Alice's `tx_bch_swaplock_swap`, extracts the `bob_allow_swap_dec` from the TX, and then uses the decrypted signature to learn Alice's `seckey_ves_alice`.

Bob then uses his `seckey_ves_bob` and `seckey_ves_alice` to construct and sign `tx_btc_swap` which will send the BTC to destination of his choice, and with this the swap is completed.

The `timelock_1` must be set up so go guarantee that Bob's `tx_btc_swap` has enough time to get mined.
If `timelock_1` would expire while transaction has not been confirmed, then Alice could attempt to replace the original transaction and spend from `tx_btc_swaplock` by using the refund path.

### Refund Path

Either party has a chance of forcing a refund by blocking the happy path and waiting for their timelock expire:

1. If Alice never funds the BTC address,
2. If Bob never reveals the `bob_allow_swap_ves`,
3. If Alice never uses the `bob_allow_swap_dec`.

Once `timelock_0` expires, anyone with knowledge of `swaplock` contract can post the `tx_bch_swaplock_refund` transaction to perform refund on BCH side.

Once `timelock_1` expires, Alice can use her `pubkey_alice_btc` and post the `tx_btc_swaplock_refund` transaction to perform refund on BTC side.

The timelocks must be set up so that `tx_btc_swaplock_refund` can not be posted before `tx_bch_swaplock_refund`, else Alice could perform the BTC refund AND claim the BCH.

## Appendix - Smart Contracts

### BCH - Swaplock

#### Unlocking Data - Happy Path

```
<bob_allow_swap_dec>
```

#### Unlocking Data - Refund Path

none

#### Redeem Script

```
// Verify that the TX is 1-in-1-out
OP_TXINPUTCOUNT OP_1 OP_NUMEQUALVERIFY
OP_TXOUTPUTCOUNT OP_1 OP_NUMEQUALVERIFY

// Verify that (amount_bch - mining_fee_0) is being sent to the output
<mining_fee_0> OP_0 OP_UTXOVALUE OP_0 OP_OUTPUTVALUE OP_SUB OP_NUMEQUALVERIFY

// Verify that any CashTokens are being sent to the output
OP_0 OP_UTXOTOKENCATEGORY OP_0 OP_OUTPUTTOKENCATEGORY OP_EQUALVERIFY
OP_0 OP_UTXOTOKENCOMMITMENT OP_0 OP_OUTPUTTOKENCOMMITMENT OP_EQUALVERIFY
OP_0 OP_UTXOTOKENAMOUNT OP_0 OP_OUTPUTTOKENAMOUNT OP_NUMEQUALVERIFY

OP_0 OP_INPUTSEQUENCENUMBER OP_NOTIF
    // Happy path
    // Verify that destination is Alice's address
    <bytecode_alice> OP_0 OP_OUTPUTBYTECODE OP_OVER OP_EQUALVERIFY
    // Verify decrypted signature
    <pubkey_ves_bob_bch> OP_CHECKDATASIG
OP_ELSE
    // Refund path
    // Verify refund timelock.
    <timelock_0> OP_CHECKSEQUENCEVERIFY OP_DROP

    // Verify that destination is Refund contract address
    <bytecode_refund> OP_0 OP_OUTPUTBYTECODE OP_EQUAL
OP_ENDIF
```

### BCH - Refund

#### Unlocking Data - Refund Path

```
<alice_allow_refund_dec>
```

#### Unlocking Data - Recover Path

none

#### Redeem Script

```
// Verify that the TX is 1-in-1-out
OP_TXINPUTCOUNT OP_1 OP_NUMEQUALVERIFY
OP_TXOUTPUTCOUNT OP_1 OP_NUMEQUALVERIFY

// Verify that (amount_bch - mining_fee_0 - mining_fee_1) is being sent to the output
<mining_fee_1> OP_0 OP_UTXOVALUE OP_0 OP_OUTPUTVALUE OP_SUB OP_NUMEQUALVERIFY

// Verify that any CashTokens are being sent to the output
OP_0 OP_UTXOTOKENCATEGORY OP_0 OP_OUTPUTTOKENCATEGORY OP_EQUALVERIFY
OP_0 OP_UTXOTOKENCOMMITMENT OP_0 OP_OUTPUTTOKENCOMMITMENT OP_EQUALVERIFY
OP_0 OP_UTXOTOKENAMOUNT OP_0 OP_OUTPUTTOKENAMOUNT OP_NUMEQUALVERIFY

OP_0 OP_INPUTSEQUENCENUMBER OP_NOTIF
    // Refund path
    // Verify that destination is Bob's address
    <bytecode_bob> OP_0 OP_OUTPUTBYTECODE OP_OVER OP_EQUALVERIFY
    // Verify decrypted signature
    <pubkey_ves_alice_bch> OP_CHECKDATASIG
OP_ELSE
    // Refund path
    // Verify recover timelock.
    <timelock_1> OP_CHECKSEQUENCEVERIFY OP_DROP

    // Verify that destination is Alice's address
    <bytecode_alice> OP_0 OP_OUTPUTBYTECODE OP_EQUAL
OP_ENDIF
```

### BTC - Swaplock

#### Unlocking Data - Happy Path

```
<1>
```

#### Unlocking Data - Refund Path

```
<0>
```

#### Redeem Script

```
OP_IF
    // Happy path
    <pubkey_ves> OP_CHECKSIG
OP_ELSE
    // Refund path
    <timelock_1> OP_CHECKSEQUENCEVERIFY OP_DROP
    <pubkey_alice_btc> OP_CHECKSIG
OP_ENDIF
```

## License

[MIT No Attribution](https://opensource.org/license/mit-0/).

## Acknowledgments

- [PHCitizen](https://github.com/PHCitizen) for helping [work out the v4](https://bitcoincashresearch.org/t/monero-bch-atomic-swaps/545/34) and creating the [1st PoC implementation](https://github.com/PHCitizen/bch-xmr-swap) in Rust ([`5d9c13db8c40b2ab29b58a1f480bc90ba0746a7512ed78ceb2467f5084c7193a`](https://chipnet.imaginary.cash/tx/5d9c13db8c40b2ab29b58a1f480bc90ba0746a7512ed78ceb2467f5084c7193a)).
- [mainnet-pat](https://github.com/mainnet-pat) for optimizing it by removing an unnecessary sha256 op (removed in v4.1) and making the 2nd PoC using mainnet-js and cashscript libraries alongside cross-compiled Rust WASM for DLEQ ([`21838d4a473ef92dd811cb4374f716e7e199b54c5c89c0033b81c5e7c942a54b`](https://explorer.bitcoinunlimited.info/tx/21838d4a473ef92dd811cb4374f716e7e199b54c5c89c0033b81c5e7c942a54b)).

## References

1. Hash Time Locked Contracts (HTLCs), https://en.bitcoin.it/wiki/Hash_Time_Locked_Contracts
2. TierNolan: Re: Alt chains and atomic transfers, https://bitcointalk.org/index.php?topic=193281.msg2224949\#msg2224949
3. On-chain atomic swaps for Decred and other cryptocurrencies, https://github.com/decred/atomicswap
4. Poelstra A.: Scriptless Scripts, https://www.sosthene.net/wp-content/uploads/wpforo/default_attachments/1562671969-Poelstra-Scriptless-Scripts.pdf
5. Fournier, L.: One-Time Verifiably Encrypted Signatures A.K.A. Adaptor Signatures, https://raw.githubusercontent.com/LLFourn/one-time-VES/master/main.pdf
6. Point Time Locked Contracts (PTLCs), https://bitcoinops.org/en/topics/ptlc/
7. A proof-of-concept implementation of a Grin-Bitcoin atomic swap, https://github.com/comit-network/grin-btc-poc
8. Gugger, J.: Bitcoin–Monero Cross-chain Atomic Swap, https://eprint.iacr.org/2020/1126.pdf
9. Hoenisch, P. & Pino, L.: Atomic Swaps between Bitcoin and Monero (2021), https://arxiv.org/pdf/2101.12332.pdf
10. BIP-141: Segregated Witness (Consensus layer), https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki
11. HF-20180820: OP_CHECKDATASIG and OP_CHECKDATASIGVERIFY Specification, https://documentation.cash/protocol/forks/op_checkdatasig.html
12. CHIP-2021-02: Native Introspection Opcodes, https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md
13. Poon, J., Dryja, T.: The Bitcoin Lightning Network: Scalable Off-Chain Instant Payments (2016), https://lightning.network/lightning-network-paper.pdf
14. Bitcoin Cash Node Technical Bulletin, Bitcoin Cash Pay-to-Script-Hash (P2SH): Past, Present, and Future, https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#322-plausibility-of-a-collision-attack
15. CHIP-2022-05 Pay-to-Script-Hash-32 (P2SH32) for Bitcoin Cash, https://gitlab.com/0353F40E/p2sh32
16. Noether, S.: Discrete logarithm equality across groups, https://web.getmonero.org/es/resources/research-lab/pubs/MRL-0010.pdf
17. BIP-341: Taproot: SegWit version 1 spending rules, https://en.bitcoin.it/wiki/BIP_0341