pragma cashscript ^0.8.0;

// v4.0.0
// Alice sent XMR, wants XMR back.
// Bob sent BCH, wants BCH back.

contract Refund(
    int miningFee,
    bytes bobOutput,
    pubkey alicePubkeyVES,
    int timelock_1,
    bytes aliceOutput
) {
    function CompleteRefundOrRecover(datasig aliceSignatureVES) {
        // Verify 1-in-1-out TX form
        require(tx.inputs.length == 1);
        require(tx.outputs.length == 1);

        // Verify pre-agreed mining fee.
        require(miningFee == tx.inputs[0].value - tx.outputs[0].value);

        // If sequence is not used then it is a regular refund TX.
        if(tx.inputs[0].sequenceNumber == 0) {
            // Verify that the BCH is forwarded to Bob's output.
            require(tx.outputs[0].lockingBytecode == bobOutput);

            // Require Bob to decrypt and publish Alice's VES signature.
            // The "message" signed is simply a sha256 hash of Bob's output
            // locking bytecode.
            // By decrypting Alice's VES and publishing it, Bob reveals his
            // XMR key share to Alice.
            require(checkDataSig(aliceSignatureVES, sha256(bobOutput), alicePubkeyVES));

            // If a TX using this path is mined then Bob gets his BCH back.
            // Alice uses the revealed XMR key share to get her XMR back.
        }
        // Else Bob must be withholding the signature and then recovering his
        // BCH will be the only possible spending path, available when
        // timelock expires.
        else {
            // Verify grace period timelock.
            require(tx.age >= timelock_1);

            // Verify that the BCH is forwarded to Alice's output.
            require(tx.outputs[0].lockingBytecode == aliceOutput);

            // Not used, require 0 to prevent a malicious party from bloating
            // the TX by pushing garbage bytes.
            require(aliceSignatureVES == 0x);

            // Alice doesn't get her XMR refund,
            // but she gets Bob's BCH instead.
        }
    }
}
